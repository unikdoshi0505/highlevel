package com.highline.framework.Test;

import BusinessFlow.AddEmployee;
import BusinessFlow.Login;
import BusinessFlow.Settings;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utility.BaseClass;
import utility.JsonUtilis;

public class CreateStaff extends BaseClass {

  Map<String,String> Data = new HashMap<>();

  @Test
  public void createStaff() throws Exception{
    Login login = new Login(driver);
    Settings settings = new Settings(driver);
    JsonUtilis jsonUtilis= new JsonUtilis();
    Data =  jsonUtilis.getData("Employee.json");
    AddEmployee addEmployee = new AddEmployee(driver);
    login.login(driver);
    settings.nevigateStaffPage();
    addEmployee.addEmployee(Data);

  }

}
