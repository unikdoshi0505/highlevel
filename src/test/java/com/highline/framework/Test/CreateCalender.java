package com.highline.framework.Test;

import BusinessFlow.AddEmployee;
import BusinessFlow.Calender;
import BusinessFlow.Login;
import BusinessFlow.Settings;
import BusinessFlow.Teams;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;
import utility.BaseClass;
import utility.JsonUtilis;

public class CreateCalender extends BaseClass {

  Map<String, String> Data = new HashMap<>();

  @Test(groups = {"reg"}, priority = 1)
  public void createCalender() throws Exception {
    Login login = new Login(driver);
    Settings settings = new Settings(driver);
    Teams teams = new Teams(driver);
    Calender calender = new Calender(driver);
    JsonUtilis jsonUtilis = new JsonUtilis();
    Data = jsonUtilis.getData("Teams.json");
    login.login(driver);
    settings.navigateTeamPage();
    teams.createTeam(Data);
    settings.navigateCalenders();
    calender.addCalender(Data);
  }
}
