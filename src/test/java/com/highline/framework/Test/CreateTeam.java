package com.highline.framework.Test;

import BusinessFlow.AddEmployee;
import BusinessFlow.Login;
import BusinessFlow.Settings;
import BusinessFlow.Teams;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;
import utility.BaseClass;
import utility.JsonUtilis;

public class CreateTeam extends BaseClass {

  Map<String,String> Data = new HashMap<>();

  @Test
  public void createTeam() throws Exception{
    Login login = new Login(driver);
    Settings settings = new Settings(driver);
    JsonUtilis jsonUtilis= new JsonUtilis();
    Data =  jsonUtilis.getData("Teams.json");
    Teams teams = new Teams(driver);
    login.login(driver);
    settings.navigateTeamPage();
    teams.createTeam(Data);
  }

}
