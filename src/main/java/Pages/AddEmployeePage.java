package Pages;

import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddEmployeePage {
  WebDriver driver;

  @FindBy(xpath="//button[contains(text(),' Add Employee ')]")
  public WebElement AddEmployee;

  @FindBy(xpath = "//*[contains(text(),' User Info ')]")
  public WebElement UserInfo;

  @FindBy(xpath = "//div[@class='modal-buttons d-flex align-items-center justify-content-between']/div")
  public WebElement Save;

  @FindBy(xpath = "//input[@placeholder='First Name']")
  public WebElement FirstName;

  @FindBy(xpath = "//input[@placeholder='Search by name, email or phone']")
  public WebElement SearchTextBox;

  @FindBy(xpath = "//input[@placeholder='Last Name']")
  public WebElement LastName;

  @FindBy(xpath = "//input[@placeholder='Email']")
  public WebElement Email;

  @FindBy(xpath = "//input[@placeholder='Password']")
  public WebElement Password;

  public AddEmployeePage(WebDriver driver){
     this.driver=driver;
    PageFactory.initElements(driver, this);
  }



}
