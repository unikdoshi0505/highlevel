package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CalenderPage {
   WebDriver driver;

   @FindBy(xpath = "//div[@id='pg-team-cal__button--create-calendar']//button")
   public WebElement addNewCalender;

   @FindBy(xpath="//*[@class='service-step step-2']")
   public WebElement availablity;

   @FindBy(xpath = "//*[@class='service-step step-3']")
   public WebElement Confirmation;

   @FindBy(xpath = "//input[@placeholder='Add Calendar Name']")
   public WebElement CalenderName;

   @FindBy(xpath = "//*[@placeholder='Add Calendar Description']")
   public WebElement CalenderDiscription;

   @FindBy(xpath = "//*[@placeholder='Enter Calendar Slug']")
   public WebElement CalenderSlug;

   @FindBy(xpath="//label[contains(text(),' Optimize for Equal Distribution ')]")
   public WebElement OptimizeForEqualDistribution;

   @FindBy(id="cmp-calmodal__button--save")
   public WebElement SaveAndContinue;

   public CalenderPage(WebDriver driver){
      this.driver=driver;
      PageFactory.initElements(driver, this);
   }

}
