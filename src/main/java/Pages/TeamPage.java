package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TeamPage {
  public WebDriver driver;

  @FindBy(xpath = "//button[contains(text(),' Add Team ')]")
  public WebElement AddTeam;

  @FindBy(xpath = "//*[contains(text(),' Team Info ')]")
  public WebElement TeamInfo;

  @FindBy(xpath = "//div[@class='form-group mt-3']/div")
  public WebElement addTeamMember;

  @FindBy(xpath = "//input[@placeholder='Add Team Name']")
  public WebElement TeamName;

  @FindBy(xpath = "//*[contains(text(),' Calendar Team Configuration ')]")
  public WebElement CalenderTeamConfiguration;

  @FindBy(xpath = "//input[@name='provider_name']")
  public WebElement providerName;

  @FindBy(xpath = "//*[@placeholder='Add Description']")
  public WebElement providerDiscreption;

  @FindBy(xpath = "//input[@name='provider_slug']")
  public WebElement providerSlug;

  @FindBy(xpath = "//div[@class='modal-footer']/div/button[contains(text(),' Save ')]")
  public WebElement Save;

  @FindBy(xpath = "//div[@class='modal-content modal-content--provider']//div//button[@class='close']")
  public WebElement closeFrame;



  public TeamPage(WebDriver driver){
    this.driver=driver;
    PageFactory.initElements(driver, this);
  }



}
