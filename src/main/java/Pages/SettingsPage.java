package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SettingsPage {
  public WebDriver driver;

  @FindBy(xpath="//*[@id=\"sb_settings\"]")
  public WebElement SetingsLink;

  @FindBy(xpath = "//span[contains(text(),' My Staff ')]")
  public WebElement MyStaffLink;

  @FindBy(id = "sb_calendar")
  public WebElement calenderLink;

  @FindBy(xpath = "//span[@class='flex items-center'][contains(text(),' Teams ')]")
  public WebElement Teams;

  public SettingsPage(WebDriver driver){
    this.driver=driver;
    PageFactory.initElements(driver, this);
  }
}
