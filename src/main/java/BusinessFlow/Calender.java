package BusinessFlow;

import Pages.CalenderPage;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import utility.LoggerClass;

public class Calender extends AbstractLayer{
  WebDriver driver;

  public Calender(WebDriver driver){
    super(driver);
    this.driver=driver;
  }

  public Calender addCalender(Map<String,String> Values){
    LoggerClass loggerClass= new LoggerClass();
    loggerClass.inializelog();
    CalenderPage calenderPage= new CalenderPage(driver);
    List<WebElement> newCalender= driver.findElements(By.id("pg-team-cal__button--create-calendar"));
    newCalender.get(1).click();
    calenderPage.CalenderName.sendKeys(Values.get("CalendarName"));
    calenderPage.CalenderDiscription.sendKeys(Values.get("CalenderDescription"));
    String calenderSlug= "Test"+getRandomNumberString();
    calenderPage.CalenderSlug.sendKeys(calenderSlug);
    calenderPage.SaveAndContinue.click();
    Assert.assertEquals("","");
    loggerClass.postLog("Calender added successfully");
    return this;
  }

}
