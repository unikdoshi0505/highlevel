package BusinessFlow;

import Pages.AddEmployeePage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class AddEmployee extends AbstractLayer {

  WebDriver driver;


  public static String getRandomNumberString() {
    // It will generate 6 digit random Number.
    // from 0 to 999999
    Random rnd = new Random();
    int number = rnd.nextInt(999999);

    // this will convert any number sequence into 6 character.
    return String.format("%06d", number);
  }

  public AddEmployee(WebDriver driver){
    super(driver);
    this.driver=driver;
  }


  public void addEmployee(Map<String,String> Values){
    AddEmployeePage addEmployeePage = new AddEmployeePage(driver);
    addEmployeePage.AddEmployee.click();
    addEmployeePage.UserInfo.click();
    addEmployeePage.FirstName.sendKeys(Values.get("FirstName"));
    addEmployeePage.LastName.sendKeys(Values.get("LastName"));
    addEmployeePage.Password.sendKeys(Values.get("Password"));
    String Email = Values.get("Email")+getRandomNumberString()+"@gmail.com";
    addEmployeePage.Email.sendKeys(Email);
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("window.scrollBy(0,700)", "");
    addEmployeePage.Save.click();
  }

}
