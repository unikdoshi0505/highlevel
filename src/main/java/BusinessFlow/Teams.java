package BusinessFlow;

import Pages.TeamPage;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Teams extends AbstractLayer{
  WebDriver driver;

  public Teams(WebDriver driver){
    super(driver);
    this.driver=driver;
  }

  public void createTeam(Map<String,String> Values){
    TeamPage teamPage= new TeamPage(driver);
    teamPage.AddTeam.click();
    teamPage.TeamInfo.click();
    teamPage.TeamName.sendKeys(Values.get("Name"));
    selectAllStaff();
    teamPage.CalenderTeamConfiguration.click();
    teamPage.providerName.sendKeys(Values.get("Name"));
    teamPage.providerDiscreption.sendKeys(Values.get("description"));
    String CalenderURL = Values.get("URL")+getRandomNumberString();
    teamPage.providerSlug.sendKeys(CalenderURL);
    teamPage.Save.click();
    teamPage.closeFrame.click();
  }

  public Teams selectAllStaff(){
    List<WebElement> stafflist = driver.findElements(By.xpath("//div[@class='space-x-1 mb-1 mt-2']//input"));
    for(int i=0;i<stafflist.size();i++){
      stafflist.get(i).click();
    }
    return this;
  }

}
