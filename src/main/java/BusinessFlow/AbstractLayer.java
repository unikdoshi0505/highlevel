package BusinessFlow;

import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractLayer {
  WebDriver driver;
  WebDriverWait wait;

  public AbstractLayer(final WebDriver driver) {
    this.driver = driver;
    wait = new WebDriverWait(driver, 2);
  }

  public Boolean waitForElementToClickable(WebElement E) {
    Boolean flag = false;
    try {
      wait.until(ExpectedConditions.elementToBeClickable(E));
      flag = true;
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Element not found");

    }
    return flag;
  }

  public void DropDownWithSearchChoice(String Choice, WebElement Element) {
    if (Choice != "" && Choice != null) {
      waitForElementToClickable(Element);
      Element.click();
      By textbox = By.xpath("//input[@role=\"textbox\"]");
      driver.findElement(textbox).sendKeys(Choice, Keys.ENTER);
    }
  }

  public static String getRandomNumberString() {
    // It will generate 6 digit random Number.
    // from 0 to 999999
    Random rnd = new Random();
    int number = rnd.nextInt(999999);

    // this will convert any number sequence into 6 character.
    return String.format("%06d", number);
  }
}
