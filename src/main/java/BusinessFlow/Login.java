package BusinessFlow;

import Pages.LoginPage;
import org.openqa.selenium.WebDriver;

public class Login extends AbstractLayer{
  WebDriver driver;

  public Login(WebDriver driver){
    super(driver);
    this.driver=driver;
  }

  public void login(WebDriver driver) {
    LoginPage loginPage= new LoginPage(driver);
    loginPage.email.sendKeys("ghlqa3@gohighlevel.com");
    loginPage.password.sendKeys("Test123!");
    loginPage.signIn.click();
  }
}
