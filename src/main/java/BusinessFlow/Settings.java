package BusinessFlow;

import Pages.SettingsPage;
import org.openqa.selenium.WebDriver;

public class Settings extends AbstractLayer{
  WebDriver driver;

  public Settings(WebDriver driver){
    super(driver);
    this.driver=driver;
  }

  public void nevigateStaffPage(){
    SettingsPage settingsPage = new SettingsPage(driver);
    settingsPage.SetingsLink.click();
    settingsPage.MyStaffLink.click();
  }

  public void navigateTeamPage(){
    SettingsPage settingsPage = new SettingsPage(driver);
    waitForElementToClickable(settingsPage.SetingsLink);
    settingsPage.SetingsLink.click();
    settingsPage.MyStaffLink.click();
    settingsPage.Teams.click();
  }

  public void navigateCalenders(){
    SettingsPage settingsPage = new SettingsPage(driver);
    //settingsPage.SetingsLink.click();
    settingsPage.calenderLink.click();
  }
}
