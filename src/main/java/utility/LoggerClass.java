package utility;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerClass {
  public static Logger logger = Logger.getLogger("ReporterLog");
  public static FileHandler filehandler;
  String workingDir = System.getProperty("user.dir");

  public void inializelog()  {
    try {
      filehandler = new FileHandler(workingDir + "\\output\\Highlavel.log", false);
      logger.addHandler(filehandler);
    }catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void postLog(String payload) {

    try {
      SimpleFormatter formatter = new SimpleFormatter();
      filehandler.setFormatter(formatter);
      logger.info(payload);

    } catch (SecurityException e) {
      e.printStackTrace();
    }
  }

}
