package utility;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverClass {

  static WebDriver driver;
  private static DriverClass obj = null;


  private DriverClass(){

  }
  public static DriverClass getInstance(){
    if(obj==null){
      obj = new DriverClass();
    }
    return obj;
  }
  public void setDriver(String browser){
    if ("chrome".equals(browser)) {
      ChromeOptions options = new ChromeOptions();
      options.addArguments("--disable-notifications");
      WebDriverManager.chromedriver().setup();
      driver = new ChromeDriver(options);
      driver.manage().window().maximize();
      driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
    }
  }

  public WebDriver getDriver(){
    return driver;
  }
}


