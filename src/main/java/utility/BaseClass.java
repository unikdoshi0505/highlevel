package utility;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseClass {

  DriverClass instance = DriverClass.getInstance();
  public WebDriver driver;

  @BeforeTest(enabled = true,alwaysRun = true)
  public void setBrowser() {
    try {
      instance.setDriver("chrome");
      driver = instance.getDriver();
      driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
      driver.get("https://app.gohighlevel.com/");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @AfterTest(enabled = true)
  public void closeBrowser() {
    driver.close();
  }


}
